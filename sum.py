def sum(a : float, b: float) -> float:
    """A sum function."""
    return a + b
